import React, { useContext } from "react";
import { Helmet } from "react-helmet";
import { Link } from "react-router-dom";
import { makeStyles } from "@material-ui/core/styles";
import { AiOutlineHome } from "react-icons/ai";
import logo from "../../assets/img/logo/fog.png";
import "./Event.css";
import { ThemeContext } from "../../contexts/ThemeContext";
import { aboutData } from "../../data/Event1Data";
import payment from "../../assets/img/payment.jpeg";

function Event5() {
  const { theme } = useContext(ThemeContext);
  const useStyles = makeStyles((t) => ({
    home: {
      color: theme.secondary,
      backgroundColor: "#FFFF",
      position: "absolute",
      top: 15,
      left: 25,
      padding: "7px",
      borderRadius: "50%",
      boxSizing: "content-box",
      fontSize: "2rem",
      cursor: "pointer",
      boxShadow:
        theme.type === "dark"
          ? "3px 3px 6px #ffffff40, -3px -3px 6px #00000050"
          : "3px 3px 6px #ffffff40, -3px -3px 6px #00000050",
      transition: "all 0.3s ease-in-out",
      "&:hover": {
        color: theme.tertiary,
        transform: "scale(1.1)",
      },
      [t.breakpoints.down("sm")]: {
        fontSize: "1.8rem",
      },
    },
    logo: {
      width: "100px",
    },
  }));

  const classes = useStyles();

  return (
    <div className="blogPage" style={{ backgroundColor: theme.secondary }}>
      <Helmet>
        <title>Fog Fashion Studio | KIDS FASHION</title>
      </Helmet>
      <div style={{ paddingBottom: "20px" }}>
        <Link style={{ backgroundColor: "#FFFF" }} to="/">
          <AiOutlineHome className={classes.home} />
        </Link>
        <img className={classes.logo} src={logo} alt="logo" />
      </div>
      <div
        className="about"
        id="about"
        style={{ backgroundColor: theme.secondary }}
      >
        <div className="about-body index">
          <div className="about-description">
            <h2 style={{ color: theme.fogYellow }}>Scan Code for Payment </h2>
          </div>
          <div className="index">
            <img src={payment} alt="payment" />
          </div>
        </div>
      </div>
    </div>
  );
}

export default Event5;
