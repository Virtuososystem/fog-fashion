import React, { useContext, useState } from "react";
import { Helmet } from "react-helmet";
import { Link } from "react-router-dom";
import { makeStyles } from "@material-ui/core/styles";
import { AiOutlineHome } from "react-icons/ai";

import "./Event.css";
import { ThemeContext } from "../../contexts/ThemeContext";
import { Snackbar, IconButton, SnackbarContent } from "@material-ui/core";
import CloseIcon from "@material-ui/icons/Close";
import emailjs from "emailjs-com";
import { AiOutlineSend, AiOutlineCheckCircle } from "react-icons/ai";
import { contactsData } from "../../data/contactsData";

function Registration ()  {
  const { theme } = useContext(ThemeContext);
  const [open, setOpen] = useState(false);
  const [name, setName] = useState("");
  const [email, setEmail] = useState("");
  const [age, setAge] = useState("");
  const [gender, setGender] = useState("");
  const [kidName, setKidName] = useState("");
  const [contactNum, setContactNum] = useState("");
  const [whatsNum, setwhatsNum] = useState("");

  const [success, setSuccess] = useState(false);
  const [errMsg, setErrMsg] = useState("");


  const useStyles = makeStyles((t) => ({
    home: {
      color: theme.secondary,
      position: "absolute",
      top: 15,
      left: 25,
      backgroundColor: "#FFFF",
      padding: "7px",
      borderRadius: "50%",
      boxSizing: "content-box",
      fontSize: "2rem",
      cursor: "pointer",
      boxShadow:
        theme.type === "dark"
          ? "3px 3px 6px #ffffff40, -3px -3px 6px #00000050"
          : "3px 3px 6px #ffffff40, -3px -3px 6px #00000050",
      transition: "all 0.3s ease-in-out",
      "&:hover": {
        color: theme.tertiary,
        transform: "scale(1.1)",
      },
      [t.breakpoints.down("sm")]: {
        fontSize: "1.8rem",
      },
    },
    input: {
      border: `4px solid ${theme.primary80}`,
      backgroundColor: `${theme.secondary}`,
      color: `${theme.tertiary}`,
      fontFamily: "var(--primaryFont)",
      fontWeight: 500,
      transition: "border 0.2s ease-in-out",
      "&:focus": {
        border: `4px solid ${theme.primary600}`,
      },
    },
    message: {
      border: `4px solid ${theme.primary80}`,
      backgroundColor: `${theme.secondary}`,
      color: `${theme.tertiary}`,
      fontFamily: "var(--primaryFont)",
      fontWeight: 500,
      transition: "border 0.2s ease-in-out",
      "&:focus": {
        border: `4px solid ${theme.primary600}`,
      },
    },
    label: {
      color: `${theme.white}`,
      backgroundColor: `${theme.secondary}`,
      fontFamily: "var(--primaryFont)",
      fontWeight: 600,
      fontSize: "0.9rem",
      padding: "0 5px",
      transform: "translate(25px,50%)",
      display: "inline-flex",
    },
    socialIcon: {
      width: "45px",
      height: "45px",
      borderRadius: "50%",
      display: "flex",
      alignItems: "center",
      justifyContent: "center",
      fontSize: "21px",
      backgroundColor: theme.primary,
      transition: "250ms ease-in-out",
      "&:hover": {
        transform: "scale(1.1)",
        color: theme.secondary,
        backgroundColor: theme.tertiary,
      },
    },
    detailsIcon: {
      backgroundColor: theme.primary,
      color: theme.secondary,
      borderRadius: "50%",
      width: "45px",
      height: "45px",
      display: "flex",
      alignItems: "center",
      justifyContent: "center",
      fontSize: "23px",
      transition: "250ms ease-in-out",
      flexShrink: 0,
      "&:hover": {
        transform: "scale(1.1)",
        color: theme.secondary,
        backgroundColor: theme.tertiary,
      },
    },

    submitBtn: {
      backgroundColor: theme.primary,
      color: theme.secondary,
      transition: "250ms ease-in-out",
      "&:hover": {
        transform: "scale(1.08)",
        color: theme.secondary,
        backgroundColor: theme.tertiary,
      },
    },
  }));

  const classes = useStyles();

  const handleClose = (event, reason) => {
    if (reason === "clickaway") {
      return;
    }

    setOpen(false);
  };
  const handleSubmit = (e) => {
    console.log("e----->", e.target);
    e.preventDefault();
    emailjs
    .sendForm(
      "service_1ehh3hk",
      "template_wj5qcuh",
      e.target,
      "user_LrfHJvXxS2chK1rJOxk5n"
    )
      .then(
        (result) => {
          console.log("success", result);
          setSuccess(true);
          setName("");
          setEmail("");
          setAge("");
          setGender("");
          setKidName("");
          setContactNum("");
          setwhatsNum("");
          setOpen(false);


        },
        (error) => {
          setErrMsg("Invalid email");
          setOpen(true);
        }
      );
  };

  return (
    <div className="blogPage" style={{ backgroundColor: theme.secondary }}>
      <Helmet>
        <title>Fog Fashion Studio | KIDS FASHION</title>
      </Helmet>
      <div style={{ paddingBottom: "50px" }}>
        <Link style={{ backgroundColor: "#FFFF" }} to="/">
          <AiOutlineHome className={classes.home} />
          <h1 className={classes.title} style={{ color: theme.white }}>
            Registration
          </h1>
        </Link>
      </div>
      <div className="about" style={{ backgroundColor: theme.secondary }}>
        <div className="contacts-form row pt-10">
          <form  onSubmit={handleSubmit}>
            {/* Parent Name */}
            <div className="input-container">
              <label htmlFor="Name" className={classes.label}>
                Parent Name
              </label>
              <input
                required
                placeholder="Enter Parent Name"
                value={name}
                onChange={(e) => setName(e.target.value)}
                type="text"
                name="Name"
                className={`form-input ${classes.input}`}
              />
            </div>

            {/* Email */}
            <div className="input-container">
              <label htmlFor="Email" className={classes.label}>
                Email
              </label>
              <input
                required
                placeholder="Enter Email Address"
                value={email}
                onChange={(e) => setEmail(e.target.value)}
                type="email"
                name="Email"
                className={`form-input ${classes.input}`}
              />
            </div>

            {/* Kid Name */}
            <div className="input-container">
              <label htmlFor="KidName" className={classes.label}>
                Kid Name
              </label>
              <input
                required
                placeholder="Enter Kid Name"
                value={kidName}
                onChange={(e) => setKidName(e.target.value)}
                type="text"
                name="kidName"
                className={`form-input ${classes.input}`}
              />
            </div>

            {/* Age */}
            <div className="input-container">
              <label htmlFor="age" className={classes.label}>
                Age
              </label>
              <select
                required
                placeholder="Enter Kid Name"
                value={age}
                onChange={(e) => setAge(e.target.value)}
                type="text"
                name="age"
                className={`form-input ${classes.input}`}
              >
                <option value="" disabled selected hidden>
                  Select Age
                </option>
                <option value="3">3 Years</option>
                <option value="4">4 Years</option>
                <option value="5">5 Years</option>
                <option value="6">6 Years</option>
                <option value="7">7 Years</option>
                <option value="8">8 Years</option>
                <option value="9">9 Years</option>
                <option value="10">10 Years</option>
                <option value="11">11 Years</option>
                <option value="12">12 Years</option>
                <option value="13">13 Years</option>
                <option value="14">14 Years</option>
                <option value="15">15 Years</option>
                <option value="16">16 Years</option>
                <option value="17">17 Years</option>
              </select>
            </div>

            {/* Gender */}
            <div className="input-container">
              <label htmlFor="gender" className={classes.label}>
                Gender
              </label>
              <select
                required
                placeholder="Enter Kid Name"
                value={gender}
                onChange={(e) => setGender(e.target.value)}
                type="text"
                name="gender"
                className={`form-input ${classes.input}`}
              >
                <option value="" disabled selected hidden>
                  Select Gender
                </option>
                <option value="Boy">Boy</option>
                <option value="Girl">Girl</option>
              </select>
            </div>

            {/* Contact Number */}
            <div className="input-container">
              <label htmlFor="contactNum" className={classes.label}>
                Contact Number
              </label>
              <input
                required
                placeholder="Enter Contact Number"
                value={contactNum}
                onChange={(e) => setContactNum(e.target.value)}
                type="number"
                name="contactNum"
                className={`form-input ${classes.input}`}
              />
            </div>

            {/* Whats App Number */}
            <div className="input-container">
              <label htmlFor="whatsNum" className={classes.label}>
                WhatsApp Number
              </label>
              <input
                required
                placeholder="Enter WhatsApp Number"
                value={whatsNum}
                onChange={(e) => setwhatsNum(e.target.value)}
                type="number"
                name="whatsNum"
                className={`form-input ${classes.input}`}
              />
            </div>
            <div
              style={{ paddingBottom: "50px" }}
              className="row input-container"
            >
              <div className="submit-btn col-6">
                <button type="submit" className={classes.submitBtn}>
                  <p style={{ paddingTop: "15px" }}>
                    {!success ? "Register" : "Registered"}
                  </p>
                  <div className="submit-icon">
                    <AiOutlineSend
                      className="send-icon"
                      style={{
                        animation: !success
                          ? "initial"
                          : "fly 0.8s linear both",
                        position: success ? "absolute" : "initial",
                      }}
                    />
                    <AiOutlineCheckCircle
                      className="success-icon"
                      style={{
                        display: !success ? "none" : "inline-flex",
                        opacity: !success ? "0" : "1",
                      }}
                    />
                  </div>
                </button>
              </div>
            </div>
          </form>
          <div className="col-12 pay index">
            <a href="https://api.whatsapp.com/send?phone=+9108618058737&text=Select%21%21%21%20%20%20Your%20Kid%20Image.">

              <button class="paybtn button w-100">
              <img
                src="https://img.icons8.com/color/48/000000/whatsapp--v3.png"
                alt="whatapp"
              />
                <span>Upload Your Image</span>
              </button>
            </a>
          </div>
          <div className="col-12 pay index">
            <a
              href={
                "https://www.instamojo.com/@fogfashionstudio/l6d7f86c39a16427e9ae2388c70581a0c/"
              }
            >
              <button class="paybtn button w-100">
                <span>Pay</span>
              </button>
            </a>
          </div>
          <Snackbar
            anchorOrigin={{
              vertical: "top",
              horizontal: "center",
            }}
            open={open}
            autoHideDuration={4000}
            onClose={handleClose}
          >
            <SnackbarContent
              action={
                <React.Fragment>
                  <IconButton
                    size="small"
                    aria-label="close"
                    color="inherit"
                    onClick={handleClose}
                  >
                    <CloseIcon fontSize="small" />
                  </IconButton>
                </React.Fragment>
              }
              style={{
                backgroundColor: theme.primary,
                color: theme.secondary,
                fontFamily: "var(--primaryFont)",
              }}
              message={errMsg}
            />
          </Snackbar>
        </div>
      </div>
    </div>
  );
};

export default Registration;
